"""
(c) Copyright 2022

Questions:
1. How often does the Bitcoin network see two consecutive blocks mined more than 2 hours apart from each other? We'd like to know your answer (it doesn't have to be precise) and your approach towards this solution using probability, and
2. How many times has the above happened so far in the history of Bitcoin?

Answers:
NOTE: Calculations are at the bottom
1. Every 2086 blocks, it will take at least 2 hours to mine the next block
2. 344 times

Approach:
- Download a sample of block data
- Use a generative model to predict blocktime as a survival function
- Calculate the expected time to success based on the model of intra-block times

Source(s):
Block Data
https://gz.blockchair.com/bitcoin/blocks/

"""

import os
import time
import tqdm
import glob
import gzip
import shutil
import logging
import requests
import threading
import scipy as sp
import numpy as np
import pandas as pd
import pyarrow as pa
import pandera as pra
import matplotlib as mp
import pyarrow.csv as pacsv
from bs4 import BeautifulSoup
from selenium import webdriver
import statsmodels.api as smapi
from concurrent import futures
from pandera.typing import DataFrame, Series
from random_user_agent.user_agent import UserAgent
from random_user_agent.params import SoftwareName, OperatingSystem
from statsmodels.distributions.empirical_distribution import ECDF


"""Plotting
"""
mp.use("TkAgg")
mp.interactive(True)
from matplotlib import pyplot as plt


"""Logging
"""
LOGFORMAT = '%(asctime)s;%(name)s;%(threadName)s;%(funcName)s;%(message)s'
logging.basicConfig(level=logging.INFO, format=LOGFORMAT)


"""Resources
"""
ROOT = os.getcwd()
DATA = os.path.join(ROOT, "data")
STAGING = os.path.join(DATA, "blocks")
RAW = os.path.join(STAGING, "raw")


"""Data
"""
base_url = "https://gz.blockchair.com/bitcoin/blocks/"


def get_links(driver):
    driver.get(base_url)
    index = BeautifulSoup(driver.page_source)
    links = driver.find_elements("xpath", "//body/pre/a")
    links = [l for l in links if "blockchair_bitcoin_blocks" in l.get_attribute("href")]
    return links


__key = None
__max_connections = 8
__delay_before_next_fetch_s = 0
__sync_lock = threading.BoundedSemaphore(value=__max_connections)
__software_names = [SoftwareName.CHROME.value]
__operating_systems = [OperatingSystem.LINUX.value,
                       OperatingSystem.WINDOWS.value]
user_agent_rotator = UserAgent(software_names=__software_names,
                               operating_systems=__operating_systems,
                               limit=__max_connections)


def get_random_user_agent():
    return user_agent_rotator.get_random_user_agent()


def fetch_block_data(l):
    # NOTE: Not used; started to throw 402's
    href = l.get("href")
    fname = os.path.splitext(href)[0]
    fp = os.path.join(RAW, fname)
    if os.path.exists(fp):
        return fp
    logging.debug("Awaiting lock...")
    try:
        with __sync_lock:
            logging.debug("Lock acquired!")
            params = {"key": __key}
            uri = "".join((base_url, href))
            logging.info(uri)
            logging.info(params)
            r = requests.get(uri, params=params)
            assert r.status_code == 200
            with open(fp, "wb") as f:
                f.write(r.content)
    except Exception as e:
        logging.error(f"Error processing: {href}")
        logging.error(str(e))
        logging.error(r.status_code)
        logging.error(r.content)
    time.sleep(__delay_before_next_fetch_s)
    return fp


def fetch_block_data_driver(l):
    uri = l.get_attribute("href")
    href = os.path.split(uri)[1]
    fp = os.path.join(RAW, href)
    if os.path.exists(fp):
        logging.debug(f"{fp} already exists!")
        return fp
    try:
        logging.debug("Downloading {href}...")
        uri = "".join((base_url, href))
        logging.debug(uri)
        l.click()
    except Exception as e:
        logging.error(f"Error processing: {href}")
        logging.error(str(e))
    return fp


def get_block_data():
    # NOTE: ran once to download all data
    # Ended up being much faster for me than downloading from bitcoin
    # blockchain network from a local / remote node daemon.

    # Get links to scrape
    # NOTE: customize profile to avoid dialog boxes when downloading
    fxProfile = webdriver.FirefoxProfile()
    fxProfile.set_preference("browser.download.dir", RAW)
    fxProfile.set_preference("browser.download.folderList", 2)
    fxProfile.set_preference("browser.helperApps.alwaysAsk.force", False)
    fxProfile.set_preference("browser.download.manager.showWhenStarting", False)
    fxProfile.set_preference("browser.helperApps.neverAsk.saveToDisk", "application/octet-stream,application/x-tar,application/x-gzip")
    driver = webdriver.Firefox(firefox_profile=fxProfile)
    links = get_links(driver)

    # Scrape links
    # NOTE: takes ~4 hours...
    executor = futures.ThreadPoolExecutor(max_workers=2)
    if not os.path.exists(RAW):
        os.makedirs(RAW)
    start_index = 0
    end_index = len(links)
    total_work_units = end_index - start_index
    for f in tqdm.tqdm(executor.map(fetch_block_data_driver,
                                    links[start_index:end_index]),
                       total=total_work_units):
        pass


"""Schema
"""

schema = pra.DataFrameSchema(
    columns={"hash": pra.Column(pra.String, required=False),
             "time": pra.Column(pra.String),
             "median_time": pra.Column(pra.String, required=False),
             "size": pra.Column(pra.Int, required=False),
             "stripped_size": pra.Column(pra.Int, required=False),
             "weight": pra.Column(pra.Int, required=False),
             "version": pra.Column(pra.Int, required=False),
             "version_hex": pra.Column(pra.String, required=False),
             "version_bits": pra.Column(pra.String, required=False),
             "merkle_root": pra.Column(pra.String, required=False),
             "nonce": pra.Column(pra.Int, required=False),
             "bits": pra.Column(pra.Int, required=False),
             "difficulty": pra.Column(pra.Float, required=False),
             "chainwork": pra.Column(pra.String, required=False),
             "coinbase_data_hex": pra.Column(pra.String, required=False),
             "transaction_count": pra.Column(pra.Int, required=False),
             "witness_count": pra.Column(pra.Int, required=False),
             "input_count": pra.Column(pra.Int, required=False),
             "output_count": pra.Column(pra.Int, required=False),
             "input_total": pra.Column(pra.Int, required=False),
             "input_total_usd": pra.Column(pra.Float, required=False),
             "output_total": pra.Column(pra.Int, required=False),
             "output_total_usd": pra.Column(pra.Float, required=False),
             "fee_total": pra.Column(pra.Int, required=False),
             "fee_total_usd": pra.Column(pra.Float, required=False),
             "fee_per_kb": pra.Column(pra.Float, required=False),
             "fee_per_kb_usd": pra.Column(pra.Float, required=False),
             "fee_per_kwu": pra.Column(pra.Float, required=False),
             "fee_per_kwu_usd": pra.Column(pra.Float, required=False),
             "cdd_total": pra.Column(pra.Float, required=False),
             "generation": pra.Column(pra.Int, required=False),
             "generation_usd": pra.Column(pra.Float, required=False),
             "reward": pra.Column(pra.Int, required=False),
             "reward_usd": pra.Column(pra.Float, required=False),
             "guessed_miner": pra.Column(pra.String, required=False),
             },
    index=pra.Index(pra.Int),  # id, NOTE; not chronological
    strict=True,
    coerce=True
)

# Concat all block data into a compressed table
consolfp = os.path.join(STAGING, "blocks.csv.gz")
dayblocks = glob.glob(os.path.join(RAW, "*.tsv.gz"))
if not os.path.exists(consolfp):
    first = True  # header switch
    csv_parse_options = pacsv.ParseOptions(delimiter="\t")
    csv_write_options = pacsv.WriteOptions(include_header=False)
    keep_col_idx = ["id", "time"]
    with pa.CompressedOutputStream(consolfp, "gzip") as out:
        for fp in tqdm.tqdm(sorted(dayblocks), total=len(dayblocks)):
            tbl = pacsv.read_csv(fp, parse_options=csv_parse_options)
            stbl = tbl.select(keep_col_idx)
            if first:
                pacsv.write_csv(stbl, out)
                first = False
            else:
                pacsv.write_csv(stbl, out, write_options=csv_write_options)

# Read blocks data into memory
df = pd.read_csv(consolfp,
                 header=0,
                 compression="infer",
                 parse_dates=["time"],
                 index_col="id")
df = df.reindex(df.sort_values("time").index, axis=0)
schema.validate(df)


"""Analysis
"""

# Compute deltas
df["delta"] = (df["time"] - df["time"].shift(periods=1)).map(lambda d: d.total_seconds() / 60)
df["delta"] = df["delta"].replace(0, 1e-6)

# NOTE: make sure all the intervals are computed correctly
e = (df["delta"] <= 0).sum()
assert e < 1

# Time Series
# NOTE: Visually inspect block-time time series
x = df.index
y = df["delta"]
plt.plot(x, y)

# KDE
# NOTE: Visually inspect empirical probability density schedule
fig = plt.figure(figsize=(12, 5))
ax = fig.add_subplot()
ax.hist(
    df["delta"],
    bins=25,
    density=True,
    label="Histogram",
    zorder=5,
    edgecolor="k",
    alpha=0.5
)

# ECDF
# NOTE: Visually inspect empirical CDF
ecdf = ECDF(df["delta"])
plt.plot(ecdf.x, ecdf.y)


"""Optimization
"""

# Winner: lognormal
# Other models evaluated: gamma, betaprime, exponential

model = sp.stats.lognorm


def ll(x, *args):
    return model.logpdf(x, *args)


def LL(theta, xs):
    # NOTE: 2**10 hyperparameter manually tuned across models
    # to be sufficiently large enough bin size to simulate pdf
    # without truncation, and without overstating the tail.
    bins, edges = np.histogram(xs, bins=2**10, range=(0.5, max(xs)))
    total = 0
    for i, cnt in enumerate(bins):
        if cnt == 0:
            continue
        ei = edges[i]
        lli = cnt * ll(ei, *theta)
        total -= lli
    return total


# NOTE: Use bins to handle large tail
perc_train = 1.0
n = int(df.shape[0] * perc_train)
theta_size = 1
x0 = [1 for _ in range(theta_size)]
args = (df.sample(n)["delta"].dropna().values,)
bounds = [(1e-6, None) for _ in x0]
method = None
tolerance = 1e-6
options = {'gtol': 1e-6, 'disp': True}
optim = sp.optimize.minimize(LL, x0, args,
                             method=method,
                             bounds=bounds,
                             tol=tolerance,
                             options=options)
theta = optim.x
logging.info(f"theta: {theta}")

# Plot pdf
# NOTE: Confirm that the shape the pdf matches empirical data
xs = np.linspace(df["delta"].dropna().min(),
                 df["delta"].dropna().max(),
                 250)
ys = [model.pdf(x, *theta) for x in xs]
plt.plot(xs, ys)


"""Answers
"""

# (2.)
actual = df.loc[df['delta'] > 120].shape[0]
p = model.sf(120, *theta)
num_blocks = 719730  # as-of 2022-01-24 09:00 AM ET
expected = p * num_blocks
logging.info(f"p[t >= 120] ~ Lognormal(*theta) := {int(expected)} vs. {actual}")
# >> p[t >= 120] ~ Lognormal(*theta) := 344 vs. 321

# (1.)
# NOTE: Success = mined >= 120 mins
# p(T<t) = SF(t)
# It then follows using E[.] of geometric distribution that it will
# take (1 - p) / p trials on average for a success to occur.
n = 0
inter_event_time = []
for d in df["delta"].dropna().values:
    if d >= 120:
        inter_event_time.append(n)
        n = 0
    else:
        n += 1
actual = np.mean(inter_event_time)
expected = (1 - p) / p
logging.info(f"E[k] ~ Geometric(p(t >= 120; *theta)) := {int(expected)} vs. {int(actual)}")
# >> E[k] ~ Geometric(p(t >= 120; *theta)) := 2086 vs. 1888
