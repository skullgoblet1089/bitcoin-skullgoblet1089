### Questions:
1. How often does the Bitcoin network see two consecutive blocks mined more than 2 hours apart from each other? We'd like to know your answer (it doesn't have to be precise) and your approach towards this solution using probability, and
1. How many times has the above happened so far in the history of Bitcoin?

### Answers:
1. Every 2086 blocks, it will take at least 2 hours to mine the next block
1. 344 times

### Approach:
- Download a sample of block data
- Use a generative model to predict blocktime as a survival function
- Calculate the expected time to success based on the model of intra-block times
- All calculations and more detailed notes explaining my work are in stakefish.py

### Source(s):

- Block Data (https://gz.blockchair.com/bitcoin/blocks/)
